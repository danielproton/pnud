<!--  
    The BackEnd software was developed on the is.gd platform
    https://is.gd/faq.php#software 
    based on php language
    FrontEnd used JQuery + HTML and CSS

https://is.gd/create.php?format=simple&url=www.example.com - para encurtar ou 
https://is.gd/create.php?format=json&callback=myfunction&url=www.example.com&shorturl=example&logstats=1
https://is.gd/forward.php?shorturl=example - para retornar a url original a partir da encurtada ou 
https://is.gd/forward.php?format=json&callback=myfunction&shorturl=example 
https://is.gd/recent.php - para ver as urls criadas
https://is.gd/b9W9Is-  = url encurtada + '-' = para ver as estatisticas
https://is.gd/usagelimits.php
-->

<?php
function url()
{
    if (isset($_POST["url"])) {
        $url = $_POST["url"];
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            //echo 'Echo: A URL eh invalida!';
            //var_dump($url);
        } else {
            //echo 'Echo: A URL eh VALIDA!';
            var_dump($_POST["url"]);
        }

    }
}
?>

<!DOCTYPE html>
<html>
<head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!--   <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script> -->

<script>

function validaURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(str);
}

function testeVal()
{
	if(validaURL(document.getElementById("url").value))
		alert("A URL eh valida");
	else
		alert("Nao deu certo!");
}

function usedURL(url)
{
  let shorturl = "";
  shorturl = getCookie(url);
  if ((shorturl === undefined) || (shorturl === "") || (shorturl === null)) {
    return false;
  }
  else
  	return true;
}

function setCookie(name, value, duration) {
	if((duration !== undefined) || (duration !== "")){
    var cookie = name + "=" + escape(value) +
    ((duration) ? "; duration=" + duration.toGMTString() : "");

    document.cookie = cookie;
  }
  else
  {
    var cookie = name + "=" + escape(value);
    document.cookie = cookie;

  }
}

function getCookie(name) {
  var cookies = document.cookie;
  var prefix = name + "=";
  var begin = cookies.indexOf("; " + prefix);

  if (begin == -1) {

    begin = cookies.indexOf(prefix);
     
    if (begin != 0) {
        return null;
    }

  } else {
      begin += 2;
  }

  var end = cookies.indexOf(";", begin);
   
  if (end == -1) {
      end = cookies.length;                        
  }

  return unescape(cookies.substring(begin + prefix.length, end));
}//fim getCookie

function getCookie2()
{
	var c = String(document.cookie).split(";");
    //var neq = k + "=";

	var d = "";
  for (var i = 0; i < c.length; i++) {
  	d = d + c[i] + "<BR>";
  	//alert("cookie2: " + d);
  }
  return d;
}

function deleteCookie(name) {
   if (getCookie(name)) {
          document.cookie = name + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT";
   }
}

function encurtadorFunc() {
	
	//getCookie2();
	//alert("getCookies:" + document.cookie);
  if(validaURL(document.getElementById("url").value))
  {
  	let url = document.getElementById("url").value;

		if(usedURL(url))
		{
			//alert("A URL foi usada");
			var cookShorturl = getCookie(url);
			
      document.getElementById("TitLink").innerHTML = "Shortlink ja gerado anteriormente:";
    	var conteudo = '<a href='+cookShorturl+'>'+cookShorturl+'</a>';
      document.getElementById("h2resp").innerHTML = conteudo;
  
      var conteudo2 = '<a href='+cookShorturl+'->'+'Estatisticas do Shortlink!'+'</a>';
      document.getElementById("estatisticas").innerHTML = conteudo2;
			
		}
		else
		{
      $.getJSON( "https://is.gd/create.php?callback=?",{
      	url: url,
      	logstats: 1,
        format: "json"
      }).done(function( data ) {
      
        let shorturl = data.shorturl;
      	console.log(shorturl);
  
      	if(shorturl!==undefined){
      	
    			//document.getElementById('encForm').submit();
        	document.getElementById("TitLink").innerHTML = "ShortLink Gerado:";
        	var conteudo = '<a href='+shorturl+'>'+shorturl+'</a>';
          document.getElementById("h2resp").innerHTML = conteudo;
  
          var conteudo2 = '<a href='+shorturl+'->'+'Estatisticas do Shortlink!'+'</a>';
          document.getElementById("estatisticas").innerHTML = conteudo2;
          
          //sleep(10000);
  				//setTimeout(, 10000); //10seg
  				//document.getElementById('encForm').submit();
  					
  				setCookie(url, shorturl);
  				
      	}
      	else 
      		document.getElementById("h2resp").innerHTML = "Erro ao gerar link";
      });
    }//fim else usedURL
  }
  else
  {
    alert("A URL informada eh invalida!");
    //document.getElementById("h2resp").innerHTML = "A URL informada eh invalida!";
    document.location.reload(true);
  }
  
  //var conteudo3 = getCookie2();//document.cookie;
  //document.getElementById("cookies").innerHTML = conteudo3;
  
}
</script>


<style>
  .cinput {
  	font-size: 20px;
  }
  .container {
  	width: 700px;
  	margin-left: auto;
  	margin-right: auto;
  	text-align: center;
  }
</style>

</head>
<body>
	<div class="container">
		<img src="pnud-logo-30.svg" width=100 height=200>
		<h2>PNUD Encurtador URL</h2>
		<BR>
		<form id="encForm" action="encurtador.php" method="post">
		<!-- <form id="encForm">   -->
			<input class="cinput" type="text" size="35" name="url" id="url"placeholder="Insira a URL a ser encurtada"> 
			<input class="cinput"	type="button" onclick="encurtadorFunc()" value="Encurtar">
		</form>
		<h2 id="TitLink"></h2>
		<h2 id="h2resp"></h2>
		<h3 id="estatisticas"></h3><BR>
		<h3><a href="https://is.gd/recent.php" target="_blank">Links Utilizados</a></h3>
		<h3 id="cookies"><a href="links.php" target="_blank">URLs Usados(Cookies)</a></h3><BR>
	</div>

</body>

</html>
